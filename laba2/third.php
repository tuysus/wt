<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>лаба 2 задание 3</title>

    <?php
        include_once "../WEB-INF/include/header.html"
    ?>
    
</head>
<body>

<?php
    include_once "../WEB-INF/include/menu-top.html"
?>

<div class="row">

    <div class="col-md-4"></div>
    <div class="col-md-1">

        <ul>
            <li>Первый пункт</li>
            <li>Второй пункт</li>
            <li>Третий пункт</li>
        </ul>

        <ul type="circle">
            <li>Первый пункт</li>
            <li>Второй пункт</li>
            <li>Третий пункт</li>
        </ul>

        <ul type="square">
            <li>Первый пункт</li>
            <li>Второй пункт</li>
            <li>Третий пункт</li>
        </ul>
    </div>

    <div class="col-md-1">
        <ol>
            <li>Первый пункт</li>
            <li>Второй пункт</li>
            <li>Третий пункт</li>
        </ol>

        <ol type="A">
            <li>Первый пункт</li>
            <li>Второй пункт</li>
            <li>Третий пункт</li>
        </ol>

        <ol type="a">
            <li>Первый пункт</li>
            <li>Второй пункт</li>
            <li>Третий пункт</li>
        </ol>

        <ol type="I">
            <li>Первый пункт</li>
            <li>Второй пункт</li>
            <li>Третий пункт</li>
        </ol>

        <ol type="i">
            <li>Первый пункт</li>
            <li>Второй пункт</li>
            <li>Третий пункт</li>
        </ol>
    </div>

    <div class="col-md-1">
        <ul>
            <li>Первый пункт</li>
                <ul>
                    <li>Первый подпункт</li>
                    <li>Второй подпункт</li>
                </ul>
            <li>Второй пункт</li>
        </ul>
    </div>

    <div class="col-md-11 text-center">
        <a href="background.html">Страница с фоновым изображением</a>
    </div>

</div>


<?php
    include_once "../WEB-INF/include/footer.html"
?>
</body>
</html>