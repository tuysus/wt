<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Лаба 2 задание 2</title>

    <?php
        include_once "../WEB-INF/include/header.html"
    ?>
    
</head>
<body style="background: #ff622e">

<?php
    include_once "../WEB-INF/include/menu-top.html"
?>

<div class="row">
    <div class="col-md-12" style="height: 200px"></div>
    <div class="col-md-4"></div>
    <div class="col-md-4 text-center" style="background: white; height: 100px">
        <br>
        <b>Пример жирного шрифта</b>
        <br>
        <u>Пример подчёркнутого шрифта</u>
        <br>
        <i>Пример курсива</i>
        <br>
        <font color="#dc143c">Пример цветного текста</font>
    </div>
    <div class="col-md-4"></div>
</div>

<?php
    include_once "../WEB-INF/include/footer.html"
?>

</body>
</html>