<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Таблица 3</title>

    <?php
        include_once "../WEB-INF/include/header.html"
    ?>  
    
</head>
<body>

<?php
    include_once "../WEB-INF/include/menu-top.html"
?>

<div class="row">
    <div class="col-md-12" style="height: 100px"></div>
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <table class="table" border="1">
            <tr>
                <td><a href="seventh.php"><img width="200" height="100" src="../resources/img/linux2.jpg"></a></td>
                <td class="text-center" style="vertical-align: middle" rowspan="2">Слева находятся 2 ссылки в виде изобржений, по нажатию на которые вы перейдёте на другие страницы</td>
            </tr>
            <tr>
                <td><a href="fourth.php"><img width="200" height="100" src="../resources/img/background.jpg"></a></td>
            </tr>
        </table>
    </div>
    <div class="col-md-4"></div>
</div>

<?php
    include_once "../WEB-INF/include/footer.html"
?>
</body>
</html>