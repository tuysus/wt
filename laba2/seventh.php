<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Таблица 2</title>

    <?php
    include_once "../WEB-INF/include/header.html"
    ?>
    
</head>
<body>

<?php
    include_once "../WEB-INF/include/menu-top.html"
?>

<div class="row">
    <div class="col-md-12" style="height: 100px"></div>
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <table class="table" align="center" border="1">
            <tr>
                <th class="text-center"><img src="../resources/img/windows.png"></th>
                <th class="text-center"><img src="../resources/img/linux.png"></th>
            </tr>
            <tr class="">
                <td class="text-center">Microsoft Windows — семейство проприетарных операционных систем корпорации Microsoft, ориентированных на применение графического интерфейса при управлении. Изначально Windows была всего лишь графической надстройкой для MS-DOS. По состоянию на август 2014 года под управлением операционных систем семейства Windows по данным ресурса NetMarketShare работает более 91% персональных компьютеров. Windows работает на платформах x86, x86-64, IA-64 и ARM. Существовали также версии для DEC Alpha, MIPS, PowerPC и SPARC.</td>
                <td class="text-center">Linux — общее название Unix-подобных операционных систем, основанных на одноимённом ядре. Ядро Linux создаётся и распространяется в соответствии с моделью разработки свободного и открытого программного обеспечения. Поэтому общее название не подразумевает какой-либо единой «официальной» комплектации Linux; они распространяются в основном бесплатно в виде различных готовых дистрибутивов, имеющих свой набор прикладных программ и уже настроенных под конкретные нужды пользователя. Первый релиз ядра системы состоялся 5 октября 1991.</td>
            </tr>
        </table>
    </div>
    <div class="col-md-4"></div>
</div>

<?php
    include_once "../WEB-INF/include/footer.html"
?>
</body>
</html>