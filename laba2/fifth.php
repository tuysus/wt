<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Страница с гиперссылкой в виде изображения</title>

    <?php
        include_once "../WEB-INF/include/header.html"
    ?>
    
</head>
<body>

<?php
    include_once "../WEB-INF/include/menu-top.html"
?>

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 text-center">
        <a href="background.html"><img width="400" height="400" src="../resources/img/link.jpg"></a>
    </div>
    <div class="col-md-4"></div>
</div>

<?php
    include_once "../WEB-INF/include/footer.html"
?>
</body>
</html>