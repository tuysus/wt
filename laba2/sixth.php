<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Страница с таблицей</title>

    <?php
        include_once "../WEB-INF/include/header.html"
    ?>
    
</head>
<body>

<?php
    include_once "../WEB-INF/include/menu-top.html"
?>

<div class="row">
<!--    <div class="col-md-12" style="height: 300px"></div>-->
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <table class="table" align="center" border="10" style="border-color: green red yellow blue; border-style: solid">
            <tr>
               <td class="text-center" style="background: crimson">&nbsp;1.0&nbsp;</td>
                <td class="text-center" style="background: yellow">&nbsp;1.1&nbsp;</td>
            </tr>
            <tr>
                <td class="text-center" style="background: green">&nbsp;2.0&nbsp;</td>
                <td class="text-center" style="background: blue">&nbsp;2.1&nbsp;</td>
            </tr>
        </table>
    </div>
    <div class="col-md-4"></div>
</div>

<?php
    include_once "../WEB-INF/include/footer.html"
?>
</body>
</html>