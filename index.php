<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Лабораторная работа №2</title>

    <?php
        include_once "WEB-INF/include/header.html"
    ?>
        
</head>
<body>

<?php
    include_once "WEB-INF/include/menu-top.html"
?>

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <h1 class="text-center"><B>Лабораторные работы</B></h1>
        <h2 class="text-center">По курсу:</h2>
        <h2 class="text-center"><B>"WT и ПКС"</B></h2>
        <h3 class="text-center">Выполнили студенты группы КТмо1-2:</h3>
        <h4 class="text-right">Ведерников А.В.</h4>
        <h4 class="text-right">Туйсус Е.В.</h4>
    </div>
    <div class="col-md-4"></div>
</div>

<?php
    include_once "WEB-INF/include/footer.html"
?>
</body>
</html>