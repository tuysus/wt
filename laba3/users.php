<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Лабораторная работа №2</title>

    <?php
        include_once "../WEB-INF/include/header.html"
    ?>

    <script src="../resources/js/page/users.js"></script>

</head>
<body>

<?php
    include_once "../WEB-INF/include/menu-top.html"
?>

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <table class="table" id="tblUsers">
            <thead>
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>E-mail</th>
                <th>Телефон</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <button id="btnAddUser" class="btn btn-default">Добавить</button>
    </div>
    <div class="col-md-4"></div>
</div>

<?php
    include_once "../WEB-INF/include/footer.html"
?>

<?php
    include "dialog/user-edit.html"
?>


</body>
</html>
