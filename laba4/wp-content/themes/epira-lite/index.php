<?php get_header(); ?>

<div class="col-a">
    
    <?php epira_get_breadcrumb(); ?>

    <div class="widget kopa-masonry-list-widget">
        
        <?php epira_get_archive_caption(); ?>
        
        <?php
        if (have_posts()):
            global $post;
            ?>

        <ul id="kopa-blog-loadmore-placeholder" data-layout="masonry" class="clearfix">
            <?php
            while (have_posts()) : the_post();
                $post_id = get_the_ID();
                $post_url = get_permalink();
                $post_title = get_the_title();
                $post_format = get_post_format();

                $first_category = epira_get_post_first_category($post_id);

                $classes = array('entry-item', 'clearfix');      
                if(in_array($post_format, array('video', 'audio', 'gallery'))){
                    $classes[] = "{$post_format}-post";
                }else{
                    $classes[] = 'standard-post';
                }
            ?>
            <li class="masonry-item">
                <article <?php post_class( $classes ); ?>>
                    <?php 
                    if(is_sticky()){
                        echo '<span class="sticky-icon"></span>';
                    }
                    ?>

                    <?php 
                    if(has_post_thumbnail()):
                        $image = epira_get_image_src($post_id, 'blog_1_thumbnail');  
                    ?>
                    <div class="entry-thumb">
                        <?php if($first_category): ?>
                            <a class="entry-categories clearfix" href="<?php echo esc_url( $first_category['url'] ); ?>"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left"><?php echo esc_attr( $first_category['name'] ); ?></span></a>
                        <?php endif;?>

                        <a href="<?php echo $post_url; ?>"><img src="<?php echo esc_url( $image  ); ?>" alt="<?php echo $post_title; ?>"></a>
                        
                        <?php 
                        $comments = get_comment_count( $post_id ); 
                        if(!empty($comments['approved'])):
                        ?>
                            <a href="<?php echo $post_url; ?>" class="entry-comments"><?php echo $comments['approved']; ?></a>
                        <?php endif;?>

                        <a class="entry-icon" href="<?php echo $post_url; ?>"><span><i></i></span></a>
                    </div>
                    <?php endif; ?>


                    <div class="entry-content">
                        <div class="meta-box">
                            <span class="entry-author clearfix"><span class="pull-left"><?php _e('By', 'epira-lite'); ?>&nbsp;</span><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="pull-left"><?php the_author_meta( 'display_name' ); ?></a></span>

                            <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left"><?php echo get_the_date(); ?></span></span>
                        </div>
                        <h3 class="entry-title"><a href="<?php echo $post_url; ?>"><?php echo $post_title; ?></a></h3>
                    </div>
                </article>    
            </li>
            <?php endwhile; ?>
        </ul>
        
        <?php endif; ?>
        
        <?php 
        if( apply_filters( 'epira_enable_archive_loadmore', true ) ){
            epira_get_load_more_button();     
        }else{
            get_template_part('pagination');
        }        
        ?>                        
                
    </div>
    <!-- widget -->
    
</div>
<!-- col-a -->

<div class="sidebar">

    <?php               

        if (is_active_sidebar('sidebar_main_top_right')){            
            dynamic_sidebar('sidebar_main_top_right');
        }                
    ?>

</div>
<!-- sidebar -->

<div class="clear"></div>


<?php
get_footer();
