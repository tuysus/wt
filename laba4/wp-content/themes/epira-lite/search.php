<?php get_header(); ?>

            <div class="col-a">

                <?php epira_get_breadcrumb(); ?>
                <!-- breadcrumb -->

                <div class="widget kopa-list-search-widget">
                    <?php
                    if (have_posts()):
                        global $post;
                        ?>

                    <ul class="clearfix">
                        <?php
                        while (have_posts()) : the_post();
                            $post_id = get_the_ID();
                            $post_url = get_permalink();
                            $post_title = get_the_title();
                            $post_format = get_post_format();

                            $classes = array('entry-item', 'clearfix');      
                            if(in_array($post_format, array('video', 'audio', 'gallery'))){
                                $classes[] = "{$post_format}-post";
                            }else{
                                $classes[] = 'standard-post';
                            }
                        ?>
                        
                        <li>
                            <article class="entry-item clearfix">
                                <h3 class="entry-title">
                                    <a href="<?php echo $post_url; ?>"><?php echo $post_title; ?></a>
                                </h3>

                                <div class="meta-box clearfix">
                                    <span class="entry-author pull-left clearfix">
                                        <span class="pull-left"><?php _e('By', 'epira-lite'); ?>&nbsp;</span>
                                        <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="pull-left"><?php the_author_meta( 'display_name' ); ?></a>
                                    </span>                                   

                                    <span class="entry-date pull-left clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left"><?php echo get_the_date(); ?></span></span>

                                    <span class="entry-categories pull-left">
                                        <?php the_category('<span>,&nbsp;</span>'); ?>
                                    </span>

                                </div>
                                <!-- meta-box -->
                                <?php the_excerpt(); ?>
                                
                            </article>    
                        </li>

                        <?php endwhile; ?>                                                
                    </ul>
                    <?php endif;?>

                    <?php get_template_part('pagination'); ?>
                    <!-- text-center -->
                    
                </div>
                <!-- widget -->
                
            </div>
            <!-- col-a -->
<!-- col-a -->

<div class="sidebar">

    <?php               

        if (is_active_sidebar('sidebar_main_top_right')){            
            dynamic_sidebar('sidebar_main_top_right');
        }                
    ?>

</div>
<!-- sidebar -->

<div class="clear"></div>


<?php 
get_footer();