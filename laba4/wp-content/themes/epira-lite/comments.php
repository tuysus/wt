<?php
if (post_password_required())
    return;
?>

<section id="comments">
    <?php
    if (have_comments()) :
        global $post;
        ?>         
        <h5><?php echo epira_get_comments_count(); ?></h5>

        <ol class="comments-list clearfix">
            <?php
            wp_list_comments(array(
                'walker' => null,
                'style' => 'ul',
                'short_ping' => true,
                'callback' => 'kopa_list_comments',
                'type' => 'all'
            ));
            ?>
        </ol>

        <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>           
            <div class="pagination kopa-comment-pagination"> 
                <?php
                paginate_comments_links(array(
                    'prev_text' => __('Previous', 'epira-lite'),
                    'next_text' => __('Next', 'epira-lite')
                ));
                ?>
            </div>
        <?php endif; ?>

        <?php if (!comments_open() && get_comments_number()) : ?>
            <blockquote><?php _e('Comments are closed.', 'epira-lite'); ?></blockquote>
        <?php endif; ?>    
        <?php
    endif;    
    ?>
    <div class="clear"></div>
</section>
<?php

kopa_comment_form();



function kopa_comment_form($args = array(), $post_id = null) {
    if (null === $post_id)
        $post_id = get_the_ID();
    $commenter = wp_get_current_commenter();
    $user = wp_get_current_user();
    $user_identity = $user->exists() ? $user->display_name : '';
    $args = wp_parse_args($args);
    if (!isset($args['format']))
        $args['format'] = current_theme_supports('html5', 'comment-form') ? 'html5' : 'xhtml';
    $req = get_option('require_name_email');
    $aria_req = ( $req ? ' aria-required="true"' : '' );
    $html5 = 'html5' === $args['format'];
    $fields = array();

    $fields['author'] = '<div class="row">';

    $fields['author'].= '<div class="col-md-4 col-sm-4 col-xs-12">';
    $fields['author'].= '<p class="input-block">';
    $fields['author'].= sprintf('<input type="text" value="%s" id="comment_name" name="author" size="30" placeholder="%s" %s>', esc_attr($commenter['comment_author']), __('Name (*)', 'epira-lite'), $aria_req);
    $fields['author'].= '</p>';
    $fields['author'].= '</div>';

    $fields['email'] = '<div class="col-md-4 col-sm-4 col-xs-12">';
    $fields['email'].= '<p class="input-block">';
    $fields['email'].= sprintf('<input type="%s" value="%s" id="comment_email" name="email" size="30" placeholder="%s" %s>', ( $html5 ? 'email' : 'text'), esc_attr($commenter['comment_author_email']), __('Email (*)', 'epira-lite'), $aria_req);
    $fields['email'].= '</p>';
    $fields['email'].= '</div>';


    $fields['url'] = '<div class="col-md-4 col-sm-4 col-xs-12">';
    $fields['url'].= '<p class="input-block">';    
    $fields['url'].= sprintf('<input type="%s" value="%s" id="comment_url" name="url" size="30" placeholder="%s" %s>', ( $html5 ? 'url' : 'text'), esc_attr($commenter['comment_author_url']), __('Website', 'epira-lite'), $aria_req);
    $fields['url'].= '</p>';
    $fields['url'].= '</div>';

    $fields['url'].= '</div>';

    $comment_field = '<p class="textarea-block">';
    $comment_field.= sprintf('<textarea name="comment" id="comment_message" rows="10" placeholder="%s" %s></textarea>', '', $aria_req);
    $comment_field.= '</p>';

    $fields = apply_filters('comment_form_default_fields', $fields);

    $defaults = array(
        'fields' => $fields,
        'comment_field' => $comment_field,
        'must_log_in' => '<p class="must-log-in">' . sprintf(__('You must be <a href="%s">logged in</a> to post a comment.', 'epira-lite'), wp_login_url(apply_filters('the_permalink', get_permalink($post_id)))) . '</p>',
        'logged_in_as' => '<p class="logged-in-as">' . sprintf(__('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'epira-lite'), get_edit_user_link(), $user_identity, wp_logout_url(apply_filters('the_permalink', get_permalink($post_id)))) . '</p>',
        'comment_notes_before' => '',
        'comment_notes_after' => '',
        'id_form' => 'comments-form',
        'id_submit' => 'submit-comment',
        'title_reply' => __('Leave a Reply', 'epira-lite'),
        'title_reply_to' => __('Leave a Reply to %s', 'epira-lite'),
        'cancel_reply_link' => __('(Cancel)', 'epira-lite'),
        'label_submit' => __('Post Comment', 'epira-lite'),
        'format' => 'xhtml',
    );
    $args = wp_parse_args($args, apply_filters('comment_form_defaults', $defaults));
    ?>
    <?php if (comments_open($post_id)) : ?>
        <?php
        do_action('comment_form_before');
        ?>
        <section id="respond">      

            <h5 class="text-uppercase text-normal clearfix">
                <?php comment_form_title($args['title_reply'], $args['title_reply_to']); ?>
                <span class="pull-right"><?php cancel_comment_reply_link($args['cancel_reply_link']); ?></span>
            </h5>    
                   

            <?php if (get_option('comment_registration') && !is_user_logged_in()) : ?>
                <?php echo $args['must_log_in']; ?>
                <?php
                do_action('comment_form_must_log_in_after');
                ?>
            <?php else : ?>            
                <form action="<?php echo site_url('/wp-comments-post.php'); ?>" method="post" id="<?php echo esc_attr($args['id_form']); ?>" class="comments-form clearfix" <?php echo $html5 ? ' novalidate' : ''; ?>>                    
                    <?php
                    do_action('comment_form_top');
                    ?>
                    <?php if (is_user_logged_in()) : ?>
                        <?php
                        echo apply_filters('comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity);
                        ?>
                        <?php
                        do_action('comment_form_logged_in_after', $commenter, $user_identity);
                        ?>
                    <?php else : ?>                        
                        <?php
                        do_action('comment_form_before_fields');
                        foreach ((array) $args['fields'] as $name => $field) {
                            echo apply_filters("comment_form_field_{$name}", $field) . "\n";
                        }
                        do_action('comment_form_after_fields');
                        ?>
                    <?php endif; ?>

                    <?php
                    echo apply_filters('comment_form_field_comment', $args['comment_field']);
                    ?>
                    
                    <?php echo $args['comment_notes_after']; ?>


                    <p class="comment-button clearfix">                             
                        <input type="submit" name="submit" class="input-submit" value="<?php echo esc_attr($args['label_submit']); ?>" id="<?php echo esc_attr($args['id_submit']); ?>">
                        <?php comment_id_fields($post_id); ?>                                                                     
                    </p>

                    <?php
                    do_action('comment_form', $post_id);
                    ?>
                </form>
            <?php endif; ?>
       </section>
        <?php
        do_action('comment_form_after');
    else :
        do_action('comment_form_comments_closed');
    endif;
}

function kopa_list_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    ?>
    <li <?php comment_class('clearfix'); ?> id="comment-<?php comment_ID(); ?>">

        <article class="comment-wrap clearfix"> 
            <div class="comment-avatar pull-left">
                <?php echo get_avatar($comment->comment_author_email, 80); ?>
            </div>

            <div class="comment-body clearfix">
                <header class="clearfix">                                
                    <div class="pull-left">
                        <h6><?php comment_author_link(); ?></h6>
                        <span class="entry-date"><?php comment_time(get_option('date_format') . ' ' . __('at', 'epira-lite') . ' ' . get_option('time_format')); ?></span>
                    </div>
                    
                    <div class="comment-button pull-right">
                        <?php comment_reply_link(array_merge($args, array('reply_text' => 'Reply', 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>                                                                                                                                                                        
                        <?php edit_comment_link('Edit','', ''); ?>                        
                    </div>

                    <div class="clear"></div>
                </header>
                
                <?php comment_text(true); ?>      

            </div><!--comment-body -->

        </article>    
                                                                    
    </li>
    <?php
}
