<!DOCTYPE html>
<html <?php language_attributes(); ?>>              
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />                   
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php wp_title('|', true, 'right'); ?></title>                
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />                       
        <?php wp_head(); ?>
    </head>    
    <body <?php body_class(); ?>>

    <header class="kopa-page-header">

        <div class="kopa-header-top">
            
            <div class="wrapper clearfix">
                
                <div class="row">
                    <?php if(1 == (int) get_theme_mod('epira_header_headline', 1) ) : ?>
                        <div class="col-md-7 col-sm-7">                             
                            <?php epira_get_headline(); ?>               
                        </div>                    
                    <?php endif; ?>

                    <div class="col-md-5 col-sm-5">
                        
                        <div class="search-nav-box">
                            <?php if(1 == (int) get_theme_mod('epira_header_searchform', 1)): ?>
                            <div class="search-box pull-left clearfix">
                                <?php get_search_form(); ?>
                                <!-- search-form -->
                            </div>
                            <!--search-box-->
                            <?php endif; ?>

                        </div>
                        <!-- search-nav-box -->

                    </div>
                    <!-- col-md-5 -->

                </div>
                <!-- row -->

            </div>
            <!-- wrapper -->

        </div>
        <!-- kopa-header-top -->
    
        <div class="kopa-header-middle">                
        <?php 
            $top_banner_image = get_theme_mod('epira_top_banner_image');
            $top_banner_url   = get_theme_mod('epira_top_banner_link');
            $top_banner_html  = get_theme_mod('epira_top_banner_custom_html');

            if($top_banner_image || $top_banner_html):
        ?>            
        <div class="wrapper clearfix">                    
            <div id="top-banner">
                <?php if($top_banner_html){ echo wp_kses_post( $top_banner_html) ; }else{ ?>
                    <a href="<?php echo esc_url($top_banner_url); ?>"><img src="<?php echo esc_url($top_banner_image); ?>" alt=""></a>
                <?php } ?>
            </div>
        </div>
        <!-- wrapper -->            
        <?php 
            endif;
        ?>
        </div>
        <!-- kopa-header-middle -->

        <div class="kopa-header-bottom">

            <div class="wrapper clearfix">
                
                <div class="left-color"></div>

                <?php if($logo = get_theme_mod('epira_main_logo_image')): ?>
                    <div id="logo-image"><a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($logo); ?>" alt=""></a></div>
                <?php endif; ?>


                <?php 
                if (has_nav_menu('main-nav')) {
                    ?>
                    <nav id="main-nav" class="pull-left">
                        <?php
                        $main_nav_args = array(
                            'theme_location' => 'main-nav',
                            'container' => false,                                
                            'menu_id' => 'main-menu',
                            'menu_class' => 'clearfix'
                            );
                        wp_nav_menu($main_nav_args);
                        ?>
                        <i class='fa fa-align-justify'></i>
                        <div class="mobile-menu-wrapper">
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'main-nav',
                                'container' => false,                                
                                'menu_id' => 'mobile-menu',
                                'menu_class' => ''                                
                            )
                        );
                        ?>
                        </div>
                    </nav>
                    <?php
                }
                ?>              
                <!-- main-nav -->


                <?php 
                $socials = epira_get_social_networks();
                ?>
                <ul class="social-links pull-right clearfix">
                    <?php 
                    foreach( $socials as  $social ): 
                        $url = get_theme_mod($social['id']);

                        if('epira_social_link_rss' == $social['id']){
                            if($url && 'HIDE' == strtoupper($url)){
                                $url = false;
                            }elseif('' == $url){
                                $url = get_bloginfo( 'rss2_url' );
                            }                           
                        }

                        if($url):
                            ?>
                            <li>
                                <a class="<?php echo esc_attr( $social['icon'] ); ?>" target="_blank" rel="nofollow" href="<?php echo esc_url( $url );  ?>"></a>
                                <div class="square-1"></div>
                                <div class="square-2"></div>
                            </li>
                        <?php 
                        endif;
                    endforeach;
                    ?>
                </ul>
                <!-- social-links -->

            </div>
            <!-- wrapper -->            
            
        </div>
        <!-- kopa-header-bottom -->

        <?php 
        if (has_nav_menu('secondary-nav')) {
            ?>
            <nav id="secondary-nav">
                <div class="wrapper">
                <?php

                $secondary_nav_args = array(
                    'theme_location' => 'secondary-nav',
                    'container' => false,                    
                    'menu_id' => 'secondary-menu',
                    'menu_class' => 'mega-menu clearfix',
                    );
                if(class_exists('Epira_MegaMenu')){
                    $secondary_nav_args['walker'] = new Epira_MegaMenu();
                }
                wp_nav_menu($secondary_nav_args);

                ?>
                
                <span class="secondary-mobile-label"><?php _e('Secondary Menu', 'epira-lite'); ?></span>
                
                <div class="secondary-mobile-menu-wrapper">
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'secondary-nav',
                        'container' => false,                    
                        'menu_id' => 'secondary-mobile-menu',
                        'menu_class' => ''
                    )
                );
                ?>
                </div>

                </div>
                <!-- wrapper -->            
            </nav>
            <!-- secondary-nav -->
            <?php
        }
        ?>

    </header>
    <!-- kopa-page-header -->

    <?php 
    global $epira_layout_setting;
    if(isset($epira_layout_setting['sidebars']['position_below_navigation'])){
        if (is_active_sidebar($epira_layout_setting['sidebars']['position_below_navigation'])){      
            add_filter('dynamic_sidebar_params', 'epira_set_sidebar_params_for_position_below_navigation');            
            dynamic_sidebar($epira_layout_setting['sidebars']['position_below_navigation']);
            remove_filter('dynamic_sidebar_params', 'epira_set_sidebar_params_for_position_below_navigation');
        }                
    }
    ?>
    
    <div id="main-content">      
        <div class="wrapper">
      