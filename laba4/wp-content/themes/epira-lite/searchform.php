<form action="<?php echo trailingslashit(home_url('/')); ?>" class="search-form pull-left clearfix" method="get">
    <input type="text" onBlur="if (this.value == '')
        this.value = this.defaultValue;" onFocus="if (this.value == this.defaultValue)
        this.value = '';" value="<?php _e('Search', 'epira-lite'); ?>" name="s" class="search-text">
    <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
</form>