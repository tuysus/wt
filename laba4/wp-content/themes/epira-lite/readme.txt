Epira Lite theme
===================================

Description: Epira theme is designed for magazine. The theme is based on KOPATHEME layout manager technique that will let you flexibility choose layout options of every pages within your site. It is very helpful when you are experimenting with visual hierarchy. You can define unlimited sidebar for widget areas, and with powerful custom widgets, the theme provides you more flexibility and ease-of-use for your site.

===================================
 Installation
===================================
    
    -------------------------
    1. Automatic Installation
    -------------------------
    Go to WP-Admin > Appearance > Themes > Install Themes > Upload and choose the theme zip folder.
    ----------------------
    2. Manual Installation
    ----------------------
    Upload the theme files to your wordpress theme folder wp-content/themes and activate the theme in your wordpress admin panel. That's it!

	To find out more about installing WordPress themes please also see the WordPress documentation.

===================================
 Theme Options
===================================

Epira Lite supports the awesome Theme Options for all theme settings. 
Go to WP-Admin > Appearance > Customize.
    - Site Title & Tagline : Change site title and tagline.
    - Navigation : Chose Mainmenu, Secondary Menu, Footer Menu.
    - Widgets : Select widget for sidebar.
    - Static Front Page : Chose frontpage. 
    - Logo : Upload main logo, footer logo.
    - Favicon: Upload favicon.
    - Top banner : Support show banner at top theme.
    - Header: Show/Hide headline, searchform. Enable/Disable sticky menu.
    - Footer: Custom footer information show at footer theme.
    - Single post: Show/Hide author, ralted posts, comments.
    - Social link.
    - Custom CSS: Custom CSS for theme.

===================================
Change log
===================================

- 1.0.7: first version. 

===================================
Font Awesome License
===================================

Font License - http://fontawesome.io
License: SIL OFL 1.1
License URI: http://scripts.sil.org/OFL
Copyright: Dave Gandy, http://fontawesome.io

Code License - http://fontawesome.io
License: MIT License
License URI: http://opensource.org/licenses/mit-license.html
Copyright: Dave Gandy, http://fontawesome.io

Brand Icons
All brand icons are trademarks of their respective owners.
The use of these trademarks does not indicate endorsement of the trademark holder by Font Awesome, nor vice versa.

===================================
Images Licenses
===================================

    --------------------
    Screenshot
    --------------------
    http://pixabay.com/en/forge-craft-hot-form-iron-550622/
    http://pixabay.com/en/computer-summary-chart-business-767776/
    http://pixabay.com/en/firefighter-fire-portrait-training-752540/
    http://pixabay.com/en/motorbike-racing-motorcycle-race-438464/
    http://pixabay.com/en/woman-girl-people-female-hand-792162/
    http://pixabay.com/en/pole-vaulter-athlete-sport-field-668972/
    http://pixabay.com/en/typewriter-book-notebook-paper-801921/

    Licensed under CC0

    --------------------
    All icons and images in epira-lite/images/ directory
    Copyright: Kopatheme
    Licensed under the GPL3
    --------------------

===================================
Javascript Licenses
===================================
    bootstrap.js
        Bootstrap v3.0.3 (http://getbootstrap.com)
 		Copyright 2013 Twitter, Inc.
 		Licensed under http://www.apache.org/licenses/LICENSE-2.0
    classie.js
        From bonzo https://github.com/ded/bonzo
        Copyright 2012, Dustin Diaz (the "Original Author")
        Licensed under the MIT licence
    excanvas.js
        Copyright 2006 Google Inc.
        Licensed under the Apache License
        http://www.apache.org/licenses/LICENSE-2.0
    custom.js
        Copyright (c) 2014 Kopatheme - kopatheme.com
        Licensed under the GPL license:
        http://www.gnu.org/licenses/gpl.html
    filtermasonry.js
        http://kopatheme.com
        Copyright (c) 2014 Kopatheme
        Licensed under the GPL license:
        http://www.gnu.org/licenses/gpl.html
    fitvids.js
        Copyright 2013, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
		Released under the WTFPL license - http://sam.zoy.org/wtfpl/
    gmaps.js
        GMaps.js v0.4.9
        http://hpneo.github.com/gmaps/
        Copyright 2013, Gustavo Leon
        Released under the MIT License.
    imagesloaded.js
        Copyright: Tomas Sardyha @Darsain and David DeSandro @desandro, http://v3.desandro.com/
        MIT License
    imgliquid.js
        Copyright (c) 2012 Alejandro Emparan (karacas) @krc_ale
        Dual licensed under the MIT and GPL licenses
        https://github.com/karacas/imgLiquid
    jsflickrfeed.js
        Copyright (C) 2009 Joel Sutherland
        Licenced under the MIT license
        http://www.newmediacampaigns.com/page/jquery-flickr-plugin
    jquery.carouFredSel-6.2.1.js
        jQuery carouFredSel 6.2.1
        Copyright (c) 2013 Fred Heusschen
        Dual licensed under the MIT and GPL licenses.
        http://en.wikipedia.org/wiki/MIT_License
        http://en.wikipedia.org/wiki/GNU_General_Public_License
    jquery.easy-pie-chart.js
        source: http://github.com/rendro/easy-pie-chart/
        autor: Robert Fleischmann
        version: 1.0.1
        Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
        and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses
    jquery.flexslider.js
        jQuery FlexSlider v2.1 - http://www.woothemes.com/flexslider/
        Copyright 2012 WooThemes
        Free to use under the GPLv2 license.
        http://www.gnu.org/licenses/gpl-2.0.html
    jquery.form.js
        jQuery Form Plugin - version: 2.94 (13-DEC-2011)
        Dual licensed under the MIT and GPL licenses:
        http://www.opensource.org/licenses/mit-license.php
        http://www.gnu.org/licenses/gpl.html
    jquery.navgoco.js
        jQuery Navgoco Menus Plugin v0.1.5 (2013-09-07)
        Copyright (c) 2013 Chris T (@tefra)
        BSD - https://github.com/tefra/navgoco/blob/master/LICENSE-BSD
    jquery.validate.js
        jQuery Validation Plugin 1.9.0
        http://bassistance.de/jquery-plugins/jquery-plugin-validation/
        Copyright (c) 2006 - 2011 Jörn Zaefferer
        Dual licensed under the MIT and GPL licenses:
        http://www.opensource.org/licenses/mit-license.php
        http://www.gnu.org/licenses/gpl.html
    masonry.pkgd.js
        getStyleProperty by kangax
        http://perfectionkills.com/feature-testing-css-properties/
        http://masonry.desandro.com
        Licensed under MIT
    modernizr.custom.js
        Modernizr 2.7.1 (Custom Build) | MIT & BSD
    owl.carousel.js
        Copyright (c) 2013 Bartosz Wojciechowski
        http://www.owlgraphic.com/owlcarousel/
        Licensed under MIT
    superfish.js
        jQuery Superfish Menu Plugin - v1.7.4
        Copyright (c) 2013 Joel Birch
        Dual licensed under the MIT and GPL licenses:
        http://www.opensource.org/licenses/mit-license.php
        http://www.gnu.org/licenses/gpl.html
    uisearch.js
        uisearch.js v1.0.0
        http://www.codrops.com
        Licensed under the MIT license.
        http://www.opensource.org/licenses/mit-license.php
    waypoints-sticky.js
        Sticky Elements Shortcut for jQuery Waypoints - v2.0.5
        Copyright (c) 2011-2014 Caleb Troughton
        Licensed under the MIT license.
        https://github.com/imakewebthings/jquery-waypoints/blob/master/licenses.txt
    waypoints.js
        jQuery Waypoints - v2.0.5
        Copyright (c) 2011-2014 Caleb Troughton
        Licensed under the MIT license.
        https://github.com/imakewebthings/jquery-waypoints/blob/master/licenses.txt


===================================
Limitation
===================================
Footer menu does not support multi level dropdown menu

Thanks!
KOPATHEME