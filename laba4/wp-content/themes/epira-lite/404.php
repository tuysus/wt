<?php get_header(); ?>

<div class="col-a">

    <?php epira_get_breadcrumb(); ?>
    <!-- breadcrumb -->

    <section class="error-404 clearfix">
        <div class="left-col">
            <p><?php _e('404', 'epira-lite'); ?></p>
        </div><!--left-col-->
        <div class="right-col">
            <h1><?php _e('Page not found...', 'epira-lite'); ?></h1>
            <p><?php _e("We're sorry, but we can't find the page you were looking for. It's probably some thing we've done wrong but now we know about it we'll try to fix it. In the meantime, try one of this options:", 'epira-lite'); ?></p>
            <ul class="arrow-list">
                <li><a href="javascript: history.go(-1);"><?php _e('Go back to previous page', 'epira-lite'); ?></a></li>
                <li><a href="<?php echo esc_url(home_url('/')); ?>"><?php _e('Go to homepage', 'epira-lite'); ?></a></li>
            </ul>
        </div><!--right-col-->
    </section><!--error-404-->
    
</div>
<!-- col-a -->

<div class="clear"></div>
            

<?php 
get_footer();