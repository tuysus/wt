<div class="clear"></div>

</div>
    <!-- wrapper -->   
</div>
    <!-- main-content -->

    <div id="bottom-sidebar">
        
        <div class="wrapper clearfix">
            <?php if($logo_footer = get_theme_mod('epira_footer_logo_image', false)): ?>
            <div id="bottom-logo">
                <a href="<?php echo esc_url( home_url() ); ?>">
                    <img src="<?php echo esc_url( $logo_footer ); ?>" alt="" />
                </a>
            </div>
            <?php endif; ?>

            <div class="widget-area-4 pull-left">

                <?php               
                    if (is_active_sidebar('sidebar_footer_left')){            
                        dynamic_sidebar('sidebar_footer_left');
                    }                
                ?>
                
                <?php 
                if (has_nav_menu('footer-nav')) {        
                    wp_nav_menu(
                        array(
                            'theme_location' => 'footer-nav',
                            'container' => 'div', 
                            'container_id' => 'footer-nav',
                            'container_class' => 'widget kopa-menu-widget',                   
                            'menu_id' => 'footer-menu',
                            'menu_class' => 'clearfix',
                            'depth' => -1
                        )
                    );                        
                }
                ?>                
            </div>
            <!-- widget-area-4 -->

            <div class="widget-area-5 pull-right">

                <?php               
                    if (is_active_sidebar('sidebar_footer_right')){            
                        dynamic_sidebar('sidebar_footer_right');
                    }                
                ?>

            </div>
            <!-- widget-area-5 -->

            <div class="clear"></div>

        </div>
        <!-- wrapper -->

    </div>
    <!-- bottom-sidebar -->

    <footer id="kopa-page-footer">

        <div class="wrapper clearfix">
            <?php if($footer_information = get_theme_mod('epira_footer_information')): ?>
                <p id="copyright" class="pull-left"><?php echo wp_kses_post( $footer_information ); ?></p>
            <?php endif; ?>


            <p id="back-top" class="pull-right">
                <a href="#top" class="clearfix"><span class="pull-left back-top-text"><?php _e('Back to top', 'epira-lite'); ?></span><span class="back-top-icon pull-left"><i class="fa fa-angle-double-up"></i></span></a>
            </p>

        </div>
        <!-- wrapper -->

    </footer>
    <!-- kopa-page-footer -->

<?php wp_footer(); ?>
</body>
</html>