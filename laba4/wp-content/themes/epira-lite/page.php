<?php get_header(); ?>

<div class="col-a">
    
    <?php epira_get_breadcrumb(); ?>
    <!-- breadcrumb -->

    <section id="kopa-page-content" class="entry-content entry-box elements-box">
         <?php 
         if (have_posts()):    
            while (have_posts()) : the_post();
                $post_id = get_the_ID();
                $post_url = get_permalink();
                $post_title = get_the_title();
                $post_format = get_post_format();

                echo '<h1 class="hide">'.$post_title.'</h1>';

                echo '<div class="clearfix">';
                the_content();
                echo '</div>';            
           
                wp_link_pages(array(
                    'before' => '<footer class="clearfix"><div class="page-links-wrapper pull-right"><div class="page-links">',
                    'after' => '</div></div></footer>',
                    'next_or_number' => 'number',                    
                    'echo' => 1,
                    'pagelink' => '<span>%</span>',
                ));
                
            endwhile;
        endif;
        ?>
    </section>
    <!-- elements-box -->
    
</div>
<!-- col-a -->

<div class="clear"></div>


<?php 
get_footer();