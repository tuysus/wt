<?php
add_action( 'widgets_init', array('Epira_Widget_Articles_Tabs', 'register'));

class Epira_Widget_Articles_Tabs extends Epira_Widget_Master{
    
    function __construct() {
        $widget_ops = array('classname' => 'kopa-tab-1-widget', 'description' => __( 'Display a articles list with tabs layout by index: recent, comment.', 'epira-lite' ));
        $control_ops = array('width' => 'auto', 'height' => 'auto');
        parent::__construct('kopa-posts-tabs', __( 'Articles Tabs', 'epira-lite' ), $widget_ops, $control_ops);
    }

    public static function register(){
        register_widget('Epira_Widget_Articles_Tabs');
    }    

    function widget($args, $instance) {
        
        extract($args);

        $instance   = wp_parse_args((array) $instance, $this->get_default());
        
        $title      = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
        
        $query      = $this->get_query($instance);

        $tabs = array(
			'recent' => __( 'Recent', 'epira-lite' ),
			'comment' => __( 'Comment', 'epira-lite' )
		);

        echo  wp_kses_post( $before_widget);

        $affix = wp_generate_password( 4, false, false );
		?>
		<div class="kopa-tab-container-2">
			<ul class="nav nav-tabs kopa-tabs-2">
			<?php 
			$loop_index = 0;
			foreach($tabs as $key => $tab): 
				$active = (0 == $loop_index) ? 'active' :  '';
				$tab_id = "{$key}-{$affix}";
			?>
				<li class="<?php echo $active; ?>"><a data-toggle="tab" href="<?php echo "#{$tab_id}"; ?>"><?php echo esc_attr( $tab ); ?></a></li>            
			<?php 
			$loop_index++;
			endforeach;
			?>
			</ul>
            <!-- nav-tabs -->               
            
            <div class="tab-content">
            <?php 
			$loop_index = 0;
			foreach($tabs as $key => $tab): 
				$active = (0 == $loop_index) ? 'tab-pane active' :  'tab-pane';
				$tab_id = "{$key}-{$affix}";
			?>
			<div id="<?php echo $tab_id; ?>" class="<?php echo $active; ?>">
                <ul class="clearfix">
                	<?php $this->get_tab_content($query, $key); ?>
                </ul>
            </div>
			<?php 
			$loop_index++;
			endforeach;
			?>
            </div>
		</div>
		<?php

		echo $after_widget;

		$content = ob_get_clean();

		echo $content;
    }

    private function get_tab_content($query, $order){

		switch ($order) {
			case 'comment':
				$query['orderby'] = 'comment_count';
				break;			
		}
		
		$result_set = new WP_Query( $query );

		if ( $result_set->have_posts() ) :
			
			$loop_index = 0;

			while ( $result_set->have_posts() ):
				$result_set->the_post();

				$post_id = get_the_ID();
				$post_title = get_the_title();
				$post_url = get_permalink();
				$post_format = get_post_format();
				?>
				<li>
                    <article class="entry-item clearfix">
                    	<?php 
	                    if(has_post_thumbnail()):
	                        $image = epira_get_image_src($post_id, 'widget_tabs');  
	                    ?>
	                        <div class="entry-thumb pull-left">
	                            <a href="<?php echo $post_url; ?>"><img src="<?php echo $image; ?>" alt=""></a>
	                        </div>
                    	<?php endif;?>

                        <div class="entry-content">
                            <div class="meta-box">
                                 <span class="entry-author clearfix"><span class="pull-left"><?php _e('By', 'epira-lite'); ?>&nbsp;</span><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="pull-left"><?php the_author_meta( 'display_name' ); ?></a></span>
                            	<span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left"><?php echo get_the_date(); ?></span></span>
                            </div>
                            <h6 class="entry-title"><a href="<?php echo $post_url;?>"><?php echo $post_title; ?></a></h6>
                        </div>
                    </article>
                </li>
				<?php
			endwhile;

		endif;

		wp_reset_postdata();
	}
}