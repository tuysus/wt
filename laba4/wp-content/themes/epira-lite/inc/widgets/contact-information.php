<?php 
add_action( 'widgets_init', array('Epira_Widget_Contact_Information', 'register'));

class Epira_Widget_Contact_Information extends Epira_Widget_Master{
    
    function __construct() {
        $widget_ops = array('classname' => 'kopa-contact-info-widget', 'description' => __( 'Display your contact information (config by theme-options)', 'epira-lite' ));
        $control_ops = array('width' => 'auto', 'height' => 'auto');
        parent::__construct('kopa-contact-information', __( 'Contact Information', 'epira-lite' ), $widget_ops, $control_ops);
    }

    public static function register(){
        register_widget('Epira_Widget_Contact_Information');
    }    

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;

		$instance['title']   = strip_tags($new_instance['title']);
		$instance['address'] = strip_tags($new_instance['address']);
		$instance['phone']   = strip_tags($new_instance['phone']);
		$instance['email']   = strip_tags($new_instance['email']);

        return $instance;
    }

    public function form($instance) {
        $default = array(
            'title' => '',
            'address' => '',
            'phone' => '',
            'email' => ''
        );
        $instance = wp_parse_args((array) $instance, $default);       
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'epira-lite'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr(strip_tags($instance['title'])); ?>" />
        </p>  

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('address')); ?>"><?php _e('Address:', 'epira-lite'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('address')); ?>" name="<?php echo esc_attr($this->get_field_name('address')); ?>" type="text" value="<?php echo esc_attr(strip_tags($instance['address'])); ?>" />
        </p> 

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('phone')); ?>"><?php _e('Phone:', 'epira-lite'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('phone')); ?>" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" type="text" value="<?php echo esc_attr(strip_tags($instance['phone'])); ?>" />
        </p> 

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php _e('Email:', 'epira-lite'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text" value="<?php echo esc_attr(strip_tags($instance['email'])); ?>" />
        </p> 

        <?php
    }

    public function widget( $args, $instance ) {

		extract( $args );

		$instance = wp_parse_args((array) $instance, $this->get_default());

		extract( $instance );

		echo wp_kses_post( $before_widget );

		$title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
		
		if($title)
			echo wp_kses_post( $before_title . $title .$after_title );				                    
		?>
        
        <?php if( $address ): ?>          
        	<div class="contact-address"><?php echo wp_kses_post( $address ); ?></div>
    	<?php endif;?>

        <div class="clearfix">
            <?php if( $phone): ?>          
            	<a href="callto:<?php echo wp_kses_post( $phone ); ?>" class="contact-phone pull-left"><?php echo wp_kses_post( $phone ); ?></a>
			<?php endif;?>
            
			<?php if( $email ): ?>          
            	<a href="mailto:<?php echo wp_kses_post( $email ); ?>" class="contact-email pull-left"><?php echo wp_kses_post( $email ); ?></a>
            <?php endif;?>
        </div>
                
		<?php
		echo wp_kses_post( $after_widget );

	}

}