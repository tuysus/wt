<?php
add_action( 'widgets_init', array('Epira_Widget_Articles_Videos', 'register'));

class Epira_Widget_Articles_Videos extends Epira_Widget_Master{
    
    function __construct() {
        $widget_ops = array('classname' => 'kopa-media-widget', 'description' => __( 'Display articles list videos.', 'epira-lite' ));
        $control_ops = array('width' => 'auto', 'height' => 'auto');
        parent::__construct('kopa-posts-videos', __( 'Articles Videos', 'epira-lite' ), $widget_ops, $control_ops);
    }

    public static function register(){
        register_widget('Epira_Widget_Articles_Videos');
    }    

    function widget($args, $instance) {
        
        extract($args);

        $instance   = wp_parse_args((array) $instance, $this->get_default());
        
        $title      = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
        
        $query      = $this->get_query($instance);

        $query['tax_query'][] = array(
            'taxonomy' => 'post_format',
            'field' => 'slug',
            'terms' => 'post-format-video'
        );  
        
        $result_set = new WP_Query($query);

        echo  wp_kses_post( $before_widget);

        if( $title )
            echo wp_kses_post( $before_title . $title . $after_title );

        if ( $result_set->have_posts() ) :
            $loop_index = 0;            
                while ( $result_set->have_posts() ):
                    $result_set->the_post();                                        
                    $post_id = get_the_ID();
                    $post_title = get_the_title();
                    $post_url = get_permalink();
                    $post_format = get_post_format();       
                                
                    if(0 === $loop_index){
                        $image = epira_get_image_src($post_id, 'widget_videos');        
                        ?>
                        <article class="last-item">
                            <?php if(has_post_thumbnail()) : ?>
                                <div class="entry-thumb">
                                    <a href="<?php echo $post_url; ?>"><img src="<?php echo $image;?>" alt="<?php echo $post_title; ?>"></a>
                                </div>
                            <?php endif; ?>
                            <div class="entry-content clearfix">
                                <h6 class="entry-title"><a href="<?php echo $post_url; ?>"><?php echo $post_title; ?></a></h6>
                                <a href="<?php echo $post_url; ?>" class="play-icon"><i class="fa fa-play"></i></a>
                            </div>
                        </article>
                        <?php
                        if($result_set->post_count > 1)
                            echo '<ul class="older-post clearfix">';
                    }else{
                        ?>
                        <li>
                            <h6 class="entry-title"><a href="<?php echo $post_url; ?>"><?php echo $post_title; ?></a></h6>
                            <a href="<?php echo $post_url; ?>" class="play-icon"><i class="fa fa-play"></i></a>
                        </li>
                        <?php
                    }                           
                    $loop_index++;      
                endwhile;
                
                if($result_set->post_count > 1)
                            echo '</ul>';                       
        endif;

        wp_reset_postdata();

        echo wp_kses_post( $after_widget );
    }

}