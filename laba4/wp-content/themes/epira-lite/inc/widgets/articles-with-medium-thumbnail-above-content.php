<?php
add_action( 'widgets_init', array('Epira_Widget_Articles_Medium_Thumbnail_Above_Content', 'register'));

class Epira_Widget_Articles_Medium_Thumbnail_Above_Content extends Epira_Widget_Master{
    
    function __construct() {
        $widget_ops = array('classname' => 'kopa-article-list-5-widget', 'description' => __( 'Display articles list with medium thumbnail above content.', 'epira-lite' ));
        $control_ops = array('width' => 'auto', 'height' => 'auto');
        parent::__construct('kopa-posts-medium-thumbnail-above-content', __( 'Articles Medium Thumbnail Above Content', 'epira-lite' ), $widget_ops, $control_ops);
    }

    public static function register(){
    	register_widget('Epira_Widget_Articles_Medium_Thumbnail_Above_Content');
    }    

    function widget($args, $instance) {
    	
        extract($args);

		$instance   = wp_parse_args((array) $instance, $this->get_default());
		
		$title      = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
		
		$query      = $this->get_query($instance);
		
		$result_set = new WP_Query($query);

        echo  wp_kses_post( $before_widget);

		if( $title )
			echo wp_kses_post( $before_title . $title . $after_title );

        if ( $result_set->have_posts() ) :
			?>
			<ul class="clearfix">
				<?php
					while ( $result_set->have_posts() ):
						$result_set->the_post();					
						
						$post_id = get_the_ID();
						$post_title = get_the_title();
						$post_url = get_permalink();
						$post_format = get_post_format();		

						$first_category = epira_get_post_first_category($post_id);								        		
						$image = epira_get_image_src($post_id, 'widget_medium_thumbnail_above_content');						
						?>

						<li>
                            <article class="entry-item">

                            	<?php if(has_post_thumbnail()): ?>
								<div class="entry-thumb">
                                	<a href="<?php echo esc_url( $post_url ); ?>"><img src="<?php echo $image;?>" alt="<?php echo $post_title; ?>"></a>
                          
                                	<?php if($first_category): ?>                                    	
                                	<a href="<?php echo esc_url( $first_category['url'] ); ?>" class="entry-categories clearfix">
                                		<i class="fa fa-file-text-o pull-left"></i>
                                		<span class="pull-left"><?php echo $first_category['name']; ?></span>
                                	</a>
                                	<?php endif;?>
                                	
                                	<?php 
		                            $comments = get_comment_count( $post_id ); 
		                            if(!empty($comments['approved'])):
		                            ?>
		                            	<a href="<?php echo $post_url; ?>" class="entry-comments">
		                            		<?php echo $comments['approved']; ?>
		                            	</a>
		                        	<?php endif;?>	                                    
                            	</div>
								<?php endif;?>		

                                <div class="entry-content">
                                    <div class="meta-box">
                                       	<span class="entry-author clearfix"><span class="pull-left"><?php _e('By', 'epira-lite') ?>&nbsp;</span><a class="pull-left" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author_meta( 'display_name' ); ?></a></span>	
                                        <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left"><?php echo get_the_date(); ?></span></span>
                                    </div>
                                    <h6 class="entry-title"><a href="<?php echo $post_url; ?>" title="<?php echo $post_title; ?>"><?php echo $post_title; ?></a></h6>	
                                </div>

                            </article>
                        </li>
						<?php
					endwhile;
				?>
				</ul>
			<?php
		endif;

        wp_reset_postdata();

        echo wp_kses_post( $after_widget );
    }

}