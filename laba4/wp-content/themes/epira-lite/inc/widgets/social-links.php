<?php 
add_action( 'widgets_init', array('Epira_Widget_Social_Links', 'register'));

class Epira_Widget_Social_Links extends Epira_Widget_Master{
    
    function __construct() {
        $widget_ops = array('classname' => 'kopa-social-links-widget', 'description' => __( 'Display your social links.', 'epira-lite' ));
        $control_ops = array('width' => 'auto', 'height' => 'auto');
        parent::__construct('kopa-social-links', __( 'Social Links', 'epira-lite' ), $widget_ops, $control_ops);
    }

    public static function register(){
        register_widget('Epira_Widget_Social_Links');
    }    

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;

		$instance['title']   = strip_tags($new_instance['title']);

        return $instance;
    }

    public function form($instance) {
        $instance = wp_parse_args((array) $instance, $this->get_default());        
        ?>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'epira-lite'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr(strip_tags($instance['title'])); ?>" />
        </p>  

        <?php
    }

    public function widget( $args, $instance ) {

		extract( $args );

		$instance = wp_parse_args((array) $instance, $this->get_default());

		extract( $instance );

		echo wp_kses_post( $before_widget );

		$socials = epira_get_social_networks();

		$title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);			                    
		?>
        
        <ul class="social-links clearfix">
			<?php if($title): ?>
				<li><?php echo $title; ?></li>
			<?php endif; ?>
			
			<?php 
			foreach( $socials as  $social ): 
                $url = get_theme_mod($social['id']);

                if('epira_social_link_rss' == $social['id']){
                    if($url && 'HIDE' == strtoupper($url)){
                        $url = false;
                    }elseif('' == $url){
                        $url = get_bloginfo( 'rss2_url' );
                    }                           
                }

                if($url):
                	?>
                	<li>
                        <a href="<?php echo esc_url( $url ); ?>" target="_blank" rel="nofollow" class="<?php echo esc_attr( $social['icon'] ); ?>"></a>
                        <div class="square-1"></div>
                        <div class="square-2"></div>
                    </li>
                	<?php
                endif;
           	endforeach;
			?>
		</ul>
                
		<?php
		echo wp_kses_post( $after_widget );

	}

}