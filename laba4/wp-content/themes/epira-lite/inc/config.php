<?php
/**
 * Change admin theme option page slug
 */
add_filter( 'kopa_menu_settings', 'epira_change_admin_page_slug' );

function epira_change_admin_page_slug( $menu ) {
	$menu['menu_slug'] = KOPA_PREFIX . 'theme_options';
	return $menu;
}

/**
 * Change suffix in export file name
 */
add_filter( 'kopa_backup_export_type_labels', 'epira_add_theme_options_backup_label' );

function epira_add_theme_options_backup_label( $labels ) {
	$labels['theme-options'] = KOPA_PREFIX;

	return $labels; 
}

/**
 * Setup featured of theme
 */

add_action('after_setup_theme', 'epira_setup', 9);

function epira_setup(){
    add_theme_support('post-thumbnails');
    add_theme_support('loop-pagination');
    add_theme_support('automatic-feed-links');

    add_filter('admin_enqueue_scripts', 'epira_admin_enqueue_script');
	add_filter('wp_enqueue_scripts', 'epira_enqueue_script');

	
	register_nav_menus(array(
		'main-nav'      => __('Main Menu', 'epira-lite'),
		'secondary-nav' => __('Secondary Menu', 'epira-lite'),
		'footer-nav'    => __('Footer Menu', 'epira-lite'),         
    ));

	if(is_admin()){

	}else{
		add_filter('body_class', 'epira_set_body_class');
		add_filter( 'wp_title', 'epira_get_site_title', 10, 2);
		add_action( 'wp_head', 'epira_print_favicon' );
	}

	if ( ! isset( $content_width ) ){ $content_width = 770; }

	add_image_size( 'widget_medium_thumbnail_above_content', 298, 198, true ); // (cropped)
	add_image_size( 'widget_videos', 340, 183, true ); // (cropped)
	add_image_size( 'widget_tabs', 152, 100, true ); // (cropped)
	add_image_size( 'blog_1_thumbnail', 237, 160, true ); // (cropped)
	add_image_size( 'single_post_featured', 770, 420, true ); // (cropped)

	# Customize API
	add_filter('kopa_customization_init_options', 'epira_init_options');

}

function epira_get_social_networks(){
	$options = array();

	$options[] = array(
			'title' =>  __( 'Facebook', 'epira-lite' ),
			'id'    => 'epira_social_link_facebook',
			'type'  => 'url',
			'icon'	=> 'fa fa-facebook',
		);	

	$options[] = array(
		'title' =>  __( 'Twitter', 'epira-lite' ),
		'id'    => 'epira_social_link_twitter',
		'type'  => 'url',
		'icon'	=> 'fa fa-twitter',
	);	

	$options[] = array(
		'title' =>  __( 'Goggle Plus', 'epira-lite' ),
		'id'    => 'epira_social_link_google_plus',
		'type'  => 'url',
		'icon'	=> 'fa fa-google-plus',
	);	

	$options[] = array(
		'title' =>  __( 'Pinterest', 'epira-lite' ),
		'id'    => 'epira_social_link_pinterest',
		'type'  => 'url',
		'icon'	=> 'fa fa-pinterest',
	);	

	$options[] = array(
		'title' =>  __( 'Instagram', 'epira-lite' ),
		'id'    => 'epira_social_link_instagram',
		'type'  => 'url',
		'icon'	=> 'fa fa-instagram',
	);	

	$options[] = array(
		'title' =>  __( 'Tumblr', 'epira-lite' ),
		'id'    => 'epira_social_link_tumblr',
		'type'  => 'url',
		'icon'	=> 'fa fa-tumblr',
	);		

	$options[] = array(
		'title' =>  __( 'Rss', 'epira-lite' ),
		'desc' 	  => __( 'enter HIDE if you want to disable this option', 'epira-lite' ),
		'id'    => 'epira_social_link_rss',
		'type'  => 'text',
		'icon'	=> 'fa fa-rss',
	);	

	return apply_filters('epira_get_social_networks', $options );
}

/**
 * Sanitize custom css
 */
add_filter( 'pre_set_theme_mod_epira_custom_css', 'epira_sanitize_custom_css' );

function epira_sanitize_custom_css( $value ) {
 return wp_strip_all_tags( $value );
}