<?php
function epira_log($message){
	if (WP_DEBUG === true) {
        if (is_array($message) || is_object($message)) {
            error_log(print_r($message, true));
        } else {
            error_log($message);
        }
    }
}

function epira_admin_enqueue_script($hook){
    $dir = get_template_directory_uri();

    if ('widgets.php' == $hook) {
        wp_enqueue_style(KOPA_PREFIX . 'widget', $dir . "/inc/assets/css/widgets.css");  
    }
}

function epira_enqueue_script(){
	global $wp_styles, $is_IE, $kopaCurrentLayout;

	$dir = get_template_directory_uri();

	wp_enqueue_style(KOPA_PREFIX . 'font-awesome', $dir . "/css/font-awesome.css");   
    wp_enqueue_style(KOPA_PREFIX . 'bootstrap', $dir . '/css/bootstrap.css', array(), NULL); 
    wp_enqueue_style(KOPA_PREFIX . 'boostrap-slider', $dir . '/css/boostrap-slider.css', array(), NULL); 
	wp_enqueue_style(KOPA_PREFIX . 'colorbox', $dir . '/css/colorbox.css', array(), NULL); 
    wp_enqueue_style(KOPA_PREFIX . 'flexslider', $dir . '/css/flexslider.css', array(), NULL); 
    wp_enqueue_style(KOPA_PREFIX . 'easy-pie-chart', $dir . '/css/jquery.easy-pie-chart.css', array(), NULL); 
    wp_enqueue_style(KOPA_PREFIX . 'navgoco', $dir . '/css/jquery.navgoco.css', array(), NULL); 
    wp_enqueue_style(KOPA_PREFIX . 'owl-carousel', $dir . '/css/owl.carousel.css', array(), NULL); 
    wp_enqueue_style(KOPA_PREFIX . 'owl-theme', $dir . '/css/owl.theme.css', array(), NULL); 
    wp_enqueue_style(KOPA_PREFIX . 'superfish', $dir . '/css/superfish.css', array(), NULL);     
    wp_enqueue_style(KOPA_PREFIX . 'style', get_stylesheet_uri(), array(), NULL);  
    wp_enqueue_style(KOPA_PREFIX . 'responsive', $dir . '/css/responsive.css', array(), NULL);     

	if ($is_IE) {
        wp_register_style(KOPA_PREFIX . 'ie', $dir . '/css/ie.css', array(), NULL);
        wp_enqueue_style(KOPA_PREFIX . 'ie');
        $wp_styles->add_data(KOPA_PREFIX . 'ie', 'conditional', 'lt IE 9');

        wp_register_style(KOPA_PREFIX . 'ie9', $dir . '/css/ie9.css', array(), NULL);
        wp_enqueue_style(KOPA_PREFIX . 'ie9');
        $wp_styles->add_data(KOPA_PREFIX . 'ie9', 'conditional', 'IE 9');
    }

	wp_enqueue_script(KOPA_PREFIX . 'modernizr', $dir . '/js/modernizr.custom.js', array('jquery'), NULL, TRUE);
	wp_enqueue_script(KOPA_PREFIX . 'bootstrap', $dir . '/js/bootstrap.js', array('jquery'), NULL, TRUE);	
	wp_enqueue_script(KOPA_PREFIX . 'custom', $dir . '/js/custom.js', array('jquery'), NULL, TRUE);

	wp_localize_script(KOPA_PREFIX . 'custom', 'kopa_variable', array(
		'url' => array(
            'template_directory_uri' => get_template_directory_uri() . '/',
            'ajax' => admin_url('admin-ajax.php')
        ),
        'template' => array(
            'post_id' => (is_singular()) ? get_queried_object_id() : 0
        ),
        'contact' => array(
            'address' => '',
            'marker' => ''
        ),        
        'i18n' => array(
            'SEARCH' => __('Search...', 'epira-lite'),
            'LOADING' => __('Loading...', 'epira-lite'),
            'LOAD_MORE' => __('Load more', 'epira-lite'),
            'LOADING' => __('Loading...', 'epira-lite'),
            'VIEW' => __('View', 'epira-lite'),
            'VIEWS' => __('Views', 'epira-lite'),
            'validate' => array(
                'form' => array(
                    'CHECKING' => __('Checking', 'epira-lite'),
                    'SUBMIT' => __('Send Message', 'epira-lite'),
                    'SENDING' => __('Sending...', 'epira-lite')
                ),
                'recaptcha' => array(
                    'INVALID' => __('Your captcha is incorrect. Please try again', 'epira-lite'),
                    'REQUIRED' => __('Captcha is required', 'epira-lite')
                ),
                'name' => array(
                    'REQUIRED' => __('Please enter your name', 'epira-lite'),
                    'MINLENGTH' => __('At least {0} characters required', 'epira-lite')
                ),
                'email' => array(
                    'REQUIRED' => __('Please enter your email', 'epira-lite'),
                    'EMAIL' => __('Please enter a valid email', 'epira-lite')
                ),
                'url' => array(
                    'REQUIRED' => __('Please enter your url', 'epira-lite'),
                    'URL' => __('Please enter a valid url', 'epira-lite')
                ),
                'message' => array(
                    'REQUIRED' => __('Please enter a message', 'epira-lite'),
                    'MINLENGTH' => __('At least {0} characters required', 'epira-lite')
                )
            )
        ),
        'use_sticky' => (int) get_theme_mod('epira_header_sticky_menu', 1)
	));

    if (is_singular()) {
        wp_enqueue_script('comment-reply');
    }

    wp_enqueue_script(KOPA_PREFIX . 'blog-frontend', $dir . '/inc/assets/js/blog-frontend.js', array('jquery'), NULL, TRUE);

}

function epira_set_body_class($classes){

    if( is_single() ){
        array_push($classes, 'kopa-single-standard', 'kopa-two-col');
    }elseif ( is_category() || is_archive() || is_home() ) {
        array_push($classes, 'kopa-blog-1', 'kopa-two-col');
    }elseif(is_page() || is_404() ){
        array_push($classes, 'kopa-full-width');
    }elseif(is_search()){
        array_push($classes, 'kopa-blog-5',' kopa-two-col');
    }

    if(is_single() || is_category() || is_tag()){    
        if ( !is_active_sidebar('sidebar_main_top_right') ){     
            array_push($classes, 'kopa-full-width');
        }
    }

	return $classes;
}

function epira_get_headline(){
	 
	 $args = apply_filters('epira_get_headline_args', array(
        'post_type' => array('post'),               
        'posts_per_page' => 10,
        'post_status' => array('publish'),
        'ignore_sticky_posts' => true
    ));

    $posts = new WP_Query($args);

    if ($posts->have_posts()):
    	?>
		<div class="kp-headline-wrapper clearfix">
            <span class="kp-headline-title">Breaking</span>
            <div class="kp-headline clearfix">                        
	    		<dl class="ticker-1 clearfix">
		    	<?php
		    	while ($posts->have_posts()):
		            $posts->the_post();

		            $post_id = get_the_ID();
		            $post_url = get_permalink();
		            $post_title = get_the_title();

		            ?>
						<dd><a href="<?php echo esc_url($post_url); ?>"><?php echo $post_title; ?></a></dd>
		            <?php

		        endwhile;
		        ?>
			    </dl>
			</div>
		</div>
        <?php
    endif;

    wp_reset_postdata();
}

function epira_get_site_title($title, $sep) {
    global $paged, $page;

    if (is_feed()) {
        return $title;
    }

    $title .= get_bloginfo('name', 'display');

    $site_description = get_bloginfo('description', 'display');
    if ($site_description && ( is_home() || is_front_page() )) {
        $title = "$title $sep $site_description";
    }

    if ($paged >= 2 || $page >= 2) {
        $title = "$title $sep " . sprintf(__('Page %s', 'epira-lite'), max($paged, $page));
    }

    return $title;
}

function epira_print_favicon(){
	if( $favicon = get_theme_mod('epira_favicon') ){
		printf( '<link rel="shortcut icon" type="image/x-icon"  href="%s">', $favicon );
	}
}

function epira_get_image_src($post_id = 0, $size = 'thumbnail') {
    $thumb = get_the_post_thumbnail($post_id, $size);
    if (!empty($thumb)) {
        $_thumb = array();
        $regex = '#<\s*img [^\>]*src\s*=\s*(["\'])(.*?)\1#im';
        preg_match($regex, $thumb, $_thumb);
        $thumb = $_thumb[2];
    }
    return $thumb;
}

function epira_get_the_excerpt($post_id = 0, $excerpt_length = 20, $before = '<p>', $after = '</p>'){
    $content = '';

    if($excerpt_length){
        $content = wp_strip_all_tags(get_post_field('post_content', $post_id));
        if($content){
            $content =  $before . wp_trim_words( $content, (int) $excerpt_length , '') . $after;
        }
    }

    return apply_filters('epira_get_the_excerpt', $content, $post_id, $excerpt_length, $before, $after);
}

function epira_get_post_first_category( $post_id ){
    $first_category = NULL;
    $terms = get_the_terms($post_id, 'category');

    if ($terms) {
        foreach($terms as $term){
            $first_category = array(
                'url' => esc_url( get_term_link($term, 'category') ),
                'name' => $term->name
            );
            break;
        }                           
    }

    return $first_category;
}

function epira_get_human_time_diff($from){
    $periods = array(
        __("second", 'epira-lite'),
        __("minute", 'epira-lite'),
        __("hour", 'epira-lite'),
        __("day", 'epira-lite'),
        __("week", 'epira-lite'),
        __("month", 'epira-lite'),
        __("year", 'epira-lite'),
        __("decade", 'epira-lite')
    );
    $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

    $now = current_time('timestamp');

    // Determine tense of date
    if ($now > $from) {
        $difference = $now - $from;
        $tense = __("ago", 'epira-lite');
    } else {
        $difference = $from - $now;
        $tense = __("from now", 'epira-lite');
    }

    for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if ($difference != 1) {
        $periods[$j].= __("s", 'epira-lite');
    }

    return "$difference $periods[$j] {$tense}";
}

function epira_get_shortcode($content, $enable_multi = false, $shortcodes = array()) {
    $media = array();
    $regex_matches = '';
    $regex_pattern = get_shortcode_regex();
    preg_match_all('/' . $regex_pattern . '/s', $content, $regex_matches);

    foreach ($regex_matches[0] as $shortcode) {
        $regex_matches_new = '';
        preg_match('/' . $regex_pattern . '/s', $shortcode, $regex_matches_new);

        if (in_array($regex_matches_new[2], $shortcodes)) :
            $media[] = array(
                'shortcode' => $regex_matches_new[0],
                'type' => $regex_matches_new[2],
                'content' => $regex_matches_new[5],
                'atts' => shortcode_parse_atts($regex_matches_new[3])
            );

            if (false == $enable_multi) {
                break;
            }
        endif;
    }

    return $media;
}

function epira_get_breadcrumb(){
    global $post, $wp_query;
    $current_class = 'current-page';
    $prefix = '';

    $breadcrumb_before = '<div class="breadcrumb clearfix">';
    $breadcrumb_before.= sprintf('<span class="breadcrumb-title">%s</span>', __('You are here', 'epira-lite'));

    $breadcrumb_after = '</div>';

    $breadcrumb_home = '<span class="home-page" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
    $breadcrumb_home.= '<a itemprop="url" href="' . home_url() . '">';
    $breadcrumb_home.= '<i class="fa fa-home"></i>&nbsp;';
    $breadcrumb_home.= '<span itemprop="title">' . __('Home', 'epira-lite') . '</span>';
    $breadcrumb_home.= '</a>';
    $breadcrumb_home.= '</span>';

    $breadcrumb = '';

    if (is_archive()) {
        if (is_tag()) {
            $term = get_term(get_queried_object_id(), 'post_tag');
            $breadcrumb.= $prefix . sprintf('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a class="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', $current_class, $term->name);
        } else if (is_category()) {
            $category_parents = get_category_parents(get_queried_object_id(), TRUE, ',');
            if(!empty( $category_parents )){
                $terms_link = explode(',', substr( $category_parents, 0, -1));
                $n = count($terms_link);
                if ($n > 1) {
                    for ($i = 0; $i < ($n - 1); $i++) {
                        $breadcrumb.= $prefix . $terms_link[$i];
                    }
                }
            }
        
            $breadcrumb.= $prefix . sprintf('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a class="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', $current_class, get_the_category_by_ID(get_queried_object_id()));
        } else if (is_year() || is_month() || is_day()) {

            $m = get_query_var('m');
            $date = array('y' => NULL, 'm' => NULL, 'd' => NULL);
            if (strlen($m) >= 4)
                $date['y'] = substr($m, 0, 4);
            if (strlen($m) >= 6)
                $date['m'] = substr($m, 4, 2);
            if (strlen($m) >= 8)
                $date['d'] = substr($m, 6, 2);
            if ($date['y'])
                if (is_year())
                    $breadcrumb.= $prefix . sprintf('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a class="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', $current_class, $date['y']);
                else
                    $breadcrumb.= $prefix . sprintf('<span class="prev-page" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', get_year_link($date['y']), $date['y']);
            if ($date['m'])
                if (is_month())
                    $breadcrumb.= $prefix . sprintf('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a class="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', $current_class, date('F', mktime(0, 0, 0, $date['m'])));
                else
                    $breadcrumb.= $prefix . sprintf('<span class="prev-page" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', get_month_link($date['y'], $date['m']), date('F', mktime(0, 0, 0, $date['m'])));
            if ($date['d'])
                if (is_day())
                    $breadcrumb.= $prefix . sprintf('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a class="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', $current_class, $date['d']);
                else
                    $breadcrumb.= $prefix . sprintf('<span class="prev-page" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', get_day_link($date['y'], $date['m'], $date['d']), $date['d']);
        }else if (is_author()) {

            $author_id = get_queried_object_id();
            $breadcrumb.= $prefix . sprintf('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a class="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', $current_class, sprintf(__('Posts created by %1$s', 'epira-lite'), get_the_author_meta('display_name', $author_id)));
        }
    } else if (is_search()) {
        $s = get_search_query();
        $c = $wp_query->found_posts;
        $breadcrumb.= $prefix . sprintf('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a class="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', $current_class, sprintf(__('Searched for "%s" return %s results', 'epira-lite'), $s, $c));
    } else if (is_singular()) {
        if (is_page()) {
            if (is_front_page()) {
                $breadcrumb = NULL;
            } else {
                $post_ancestors = get_post_ancestors($post);
                if ($post_ancestors) {
                    $post_ancestors = array_reverse($post_ancestors);
                    foreach ($post_ancestors as $crumb)
                        $breadcrumb.= $prefix . sprintf('<span class="prev-page" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', get_permalink($crumb), esc_html(get_the_title($crumb)));
                }
                $breadcrumb.= $prefix . sprintf('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a class="%1$s" itemprop="url" href="%2$s"><span itemprop="title">%3$s</span></a></span>', $current_class, get_permalink(get_queried_object_id()), esc_html(get_the_title(get_queried_object_id())));
            }
        } else if (is_single()) {
            $categories = get_the_category(get_queried_object_id());
            if ($categories) {
                foreach ($categories as $category) {
                    $breadcrumb.= $prefix . sprintf('<span class="prev-page" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', get_category_link($category->term_id), $category->name);
                }
            }
            $breadcrumb.= $prefix . sprintf('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a class="%1$s" itemprop="url" href="%2$s"><span itemprop="title">%3$s</span></a></span>', $current_class, get_permalink(get_queried_object_id()), esc_html(get_the_title(get_queried_object_id())));
        }
    } else if (is_404()) {
        $breadcrumb.= $prefix . sprintf('<span  itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a class="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', $current_class, __('Page not found', 'epira-lite'));
    } else {
        $breadcrumb.= $prefix . sprintf('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a class="%1$s" itemprop="url"><span itemprop="title">%2$s</span></a></span>', $current_class, __('Latest News', 'epira-lite'));
    }


    echo $breadcrumb_before;
    echo $breadcrumb_home . apply_filters('epira_get_breadcrumb', $breadcrumb, $current_class, $prefix);
    echo $breadcrumb_after;
}

function epira_get_archive_caption(){            
    $caption = NULL;
    if(is_tag() || is_category()){
        $tax = get_queried_object();        
        $caption = $tax->name;        
    }else if(is_home()){
        $caption = __('Latest News', 'epira-lite');
    }

    if($caption):
    ?>
        <h2 class="widget-title widget-title-style-10">
            <span class="text-title"><?php echo esc_attr( $caption ); ?></span>
            <span class="border-top"></span>
            <span class="border-bottom"></span>
        </h2>
        <!-- widget-title -->
    <?php endif;
}

function epira_get_about_author() {
    global $post;

    $user_id = $post->post_author;
    $description = get_the_author_meta('description', $user_id);
    $email = get_the_author_meta('user_email', $user_id);
    $name = get_the_author_meta('display_name', $user_id);
    $url = get_the_author_meta('user_url', $user_id);
    $link = ($url) ? $url : get_author_posts_url($user_id);
    
    ?>

    <section class="about-author clearfix">

        <div class="author-avatar pull-left">
            <a href="<?php esc_url( $link ); ?>" rel="nofollow"><?php echo get_avatar($email, 110); ?></a>
        </div>
        
        <div class="author-content">
            <h5><?php _e('Written by', 'epira-lite'); ?>&nbsp;<a href="<?php esc_url( $link ); ?>"  rel="nofollow"><?php echo $name; ?></a></h5>
            <p><?php echo esc_textarea( $description ); ?></p>
        </div>

    </section>    

    <?php
}

function epira_get_related_posts($limit = 3){        
    if ($limit > 0) {
        ob_start();

        global $post;
        $taxs = array();

        $tags = get_the_tags($post->ID);

        if ($tags) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids[] = $tag->term_id;
            }
            $taxs [] = array(
                'taxonomy' => 'post_tag',
                'field' => 'id',
                'terms' => $ids
            );
        }

        if ($taxs) {
            $args = array(
                'post_type' => array($post->post_type),
                'tax_query' => $taxs,
                'post__not_in' => array($post->ID),
                'posts_per_page' => $limit
            );

            $result_set = new WP_Query($args);
            if ($result_set->have_posts()):
                ?>                
                <ul class="clearfix">
                    <?php
                    while ( $result_set->have_posts() ){
                        $result_set->the_post();
                            
                        $post_id = get_the_ID();
                        $post_title = get_the_title();
                        $post_url = get_permalink();                        

                        ?>
                        <li><a href="<?php echo $post_url; ?>"><?php echo $post_title; ?></a></li>
                        <?php
                    }
                    ?>
                </ul>
                <?php
            endif;

            wp_reset_postdata();
        }

        $content = ob_get_clean();
    }

    return $content;
}

function epira_get_latest_posts_by_cat($limit = 3){
    if ($limit > 0) {
        ob_start();

        global $post;
        $taxs = array();

        $cats = get_the_category(($post->ID));
        if ($cats) {
            $ids = array();
            foreach ($cats as $cat) {
                $ids[] = $cat->term_id;
            }
            $taxs [] = array(
                'taxonomy' => 'category',
                'field' => 'id',
                'terms' => $ids
            );
        }

        if ($taxs) {
            $args = array(
                'post_type' => array($post->post_type),
                'tax_query' => $taxs,
                'post__not_in' => array($post->ID),
                'posts_per_page' => $limit
            );

            $result_set = new WP_Query($args);
            if ($result_set->have_posts()):
                ?>                
                <ul class="clearfix">
                    <?php
                    while ( $result_set->have_posts() ){
                        $result_set->the_post();
                            
                        $post_id = get_the_ID();
                        $post_title = get_the_title();
                        $post_url = get_permalink();                        

                        ?>
                        <li><a href="<?php echo $post_url; ?>"><?php echo $post_title; ?></a></li>
                        <?php
                    }
                    ?>
                </ul>
                <?php
            endif;

            wp_reset_postdata();
        }

        $content = ob_get_clean();
    }

    return $content;
}

function epira_get_comments_count($post_id = 0, $return_only_number = false) {
    global $post;

    $post_id = $post_id ? $post_id : $post->ID;
    
    $count = NULL;

    if (comments_open($post_id)) {
        $comment_number = (int) get_comments_number($post_id);
        switch ($comment_number) {
            case 0:
                $count = __('Comment Now', 'epira-lite');
                break;
            case 1:
                $count = sprintf('%s %s', $comment_number, __('Comment', 'epira-lite'));
                break;
            default:
                $count = sprintf('%s %s', $comment_number, __('Comments', 'epira-lite'));
                break;
        }
    } else {
        $count = __('0 Comment', 'epira-lite');
    }

    if ($return_only_number) {
        $count = (int) $count;
    }

    return $count;
}

function epira_get_load_more_button(){    
    global  $wp, $wp_rewrite;
    
    $url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );        

    $current = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;

    if ($wp_rewrite->using_permalinks()) {
        $url = user_trailingslashit(trailingslashit(remove_query_arg('s', get_pagenum_link(1))) . 'page/', 'paged=');
    } else {
        $url = sprintf('%s&paged=', $url);
    }
    ?>

    <footer id="kopa-blog-loadmore" class="text-center" data-url="<?php echo esc_url( $url ); ?>" data-paged="<?php echo $current + 1; ?>">
        <a href="#" class="load-more"><?php _e( 'Continue reading', 'epira-lite' ); ?></a>
    </footer>
    <?php
}

function epira_adjust_color_lighten_darken($color_code, $percentage_adjuster = 0) {
    $percentage_adjuster = round($percentage_adjuster / 100, 2);
    if (is_array($color_code)) {
        $r = $color_code["r"] - (round($color_code["r"]) * $percentage_adjuster);
        $g = $color_code["g"] - (round($color_code["g"]) * $percentage_adjuster);
        $b = $color_code["b"] - (round($color_code["b"]) * $percentage_adjuster);

        return array("r" => round(max(0, min(255, $r))),
            "g" => round(max(0, min(255, $g))),
            "b" => round(max(0, min(255, $b))));
    } else if (preg_match("/#/", $color_code)) {
        $hex = str_replace("#", "", $color_code);
        $r = (strlen($hex) == 3) ? hexdec(substr($hex, 0, 1) . substr($hex, 0, 1)) : hexdec(substr($hex, 0, 2));
        $g = (strlen($hex) == 3) ? hexdec(substr($hex, 1, 1) . substr($hex, 1, 1)) : hexdec(substr($hex, 2, 2));
        $b = (strlen($hex) == 3) ? hexdec(substr($hex, 2, 1) . substr($hex, 2, 1)) : hexdec(substr($hex, 4, 2));
        $r = round($r - ($r * $percentage_adjuster));
        $g = round($g - ($g * $percentage_adjuster));
        $b = round($b - ($b * $percentage_adjuster));

        return "#" . str_pad(dechex(max(0, min(255, $r))), 2, "0", STR_PAD_LEFT)
                . str_pad(dechex(max(0, min(255, $g))), 2, "0", STR_PAD_LEFT)
                . str_pad(dechex(max(0, min(255, $b))), 2, "0", STR_PAD_LEFT);
    }
}