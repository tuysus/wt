<?php

add_action('wp_enqueue_scripts', 'epira_customize_logo', 20);
add_action('wp_enqueue_scripts', 'epira_customize_css_enqueue_scripts', 20);

function epira_customize_logo(){
    $logo_css = '';
    $margins = array(
        'margin-left',
        'margin-right',
        'margin-top',
        'margin-bottom');

    foreach($margins as $margin){
        $tmp_key = "epira_main_logo_{$margin}";
        $tmp_value = (int) get_theme_mod($tmp_key);
        if( $tmp_value ){
            $logo_css .= "{$margin} : {$tmp_value}px; ";
        }
    }

    if( !empty($logo_css) ){
        $logo_css = "#logo-image img {{$logo_css}}";
        wp_add_inline_style(KOPA_PREFIX . 'style', $logo_css);
    }

}

function epira_customize_css_enqueue_scripts(){
    if($css = get_theme_mod('epira_custom_css')){
        wp_add_inline_style(KOPA_PREFIX . 'style', $css);
    }
}

function epira_init_options($options){

    #Sections
    $options['sections'][] = array(
    'id'    => 'epira_section_logo',
    'title' => __('Logo', 'epira-lite'));

    $options['sections'][] = array(
    'id'    => 'epira_section_favicon',
    'title' => __('Favicon', 'epira-lite'));

    $options['sections'][] = array(
    'id'    => 'epira_section_top_banner',
    'title' => __('Top banner', 'epira-lite'));

    $options['sections'][] = array(
    'id'    => 'epira_section_header',
    'title' => __('Header', 'epira-lite'));

    $options['sections'][] = array(
    'id'    => 'epira_section_footer',
    'title' => __('Footer', 'epira-lite'));

    $options['sections'][] = array(
    'id'    => 'epira_section_single_post',
    'title' => __('Single post', 'epira-lite'));

    $options['sections'][] = array(
    'id'    => 'epira_section_social_link',
    'title' => __('Social link', 'epira-lite'));

    $options['sections'][] = array(
    'id'    => 'epira_section_custom_css',
    'title' => __('Custom CSS', 'epira-lite'));

    # Logo & Favicon
    $options['settings'][] = array(
        'settings'    => 'epira_main_logo_image',
        'label'       => __('Main logo image', 'epira-lite'),
        'description' => __('Upload main logo', 'epira-lite'),
        'default'     => '',
        'type'        => 'image',
        'section'     => 'epira_section_logo',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_main_logo_margin-top',
        'label'       => __('Margin top (px)', 'epira-lite'),
        'description' => __('Margin top for main logo.', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_logo',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_main_logo_margin-bottom',
        'label'       => __('Margin bottom (px)', 'epira-lite'),
        'description' => __('Margin bottom for main logo.', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_logo',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_main_logo_margin-left',
        'label'       => __('Margin left (px)', 'epira-lite'),
        'description' => __('Margin left for main logo.', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_logo',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_main_logo_margin-right',
        'label'       => __('Margin right (px)', 'epira-lite'),
        'description' => __('Margin right for main logo.', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_logo',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_footer_logo_image',
        'label'       => __('Footer logo image', 'epira-lite'),
        'description' => __('Upload footer logo', 'epira-lite'),
        'default'     => '',
        'type'        => 'image',
        'section'     => 'epira_section_logo',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_favicon',
        'label'       => __('Favicon', 'epira-lite'),
        'description' => __('Upload your favicon image', 'epira-lite'),
        'default'     => '',
        'type'        => 'image',
        'section'     => 'epira_section_favicon',
        'transport'   => 'refresh');

    # Top banner
    $options['settings'][] = array(
        'settings'    => 'epira_top_banner_image',
        'label'       => __('Top banner', 'epira-lite'),
        'default'     => '',
        'type'        => 'image',
        'section'     => 'epira_section_top_banner',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_top_banner_link',
        'label'       => __('Link to', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_top_banner',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_top_banner_custom_html',
        'label'       => __('Custom HTML', 'epira-lite'),
        'description' => __('e.g. your google adsense code, ...', 'epira-lite'),
        'default'     => '',
        'type'        => 'textarea',
        'section'     => 'epira_section_top_banner',
        'transport'   => 'refresh');

    # Header
    $options['settings'][] = array(
        'settings'    => 'epira_header_headline',
        'label'       => __('Show headline', 'epira-lite'),
        'default'     => '1',
        'type'        => 'checkbox',
        'section'     => 'epira_section_header',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_header_searchform',
        'label'       => __('Show searchform', 'epira-lite'),
        'default'     => '1',
        'type'        => 'checkbox',
        'section'     => 'epira_section_header',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_header_sticky_menu',
        'label'       => __('Use sticky menu', 'epira-lite'),
        'default'     => '1',
        'type'        => 'checkbox',
        'section'     => 'epira_section_header',
        'transport'   => 'refresh');

    # Footer
    $options['settings'][] = array(
        'settings'    => 'epira_footer_information',
        'label'       => __('Footer information', 'epira-lite'),
        'default'     => '',
        'type'        => 'textarea',
        'section'     => 'epira_section_footer',
        'transport'   => 'refresh');

    # Single Post
    $options['settings'][] = array(
        'settings'    => 'epira_single_post_author_box',
        'label'       => __('Author box', 'epira-lite'),
        'default'     => '1',
        'type'        => 'checkbox',
        'section'     => 'epira_section_single_post',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_single_post_related_posts',
        'label'       => __('Related posts', 'epira-lite'),
        'default'     => '1',
        'type'        => 'checkbox',
        'section'     => 'epira_section_single_post',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_single_post_comments',
        'label'       => __('Comments', 'epira-lite'),
        'default'     => '1',
        'type'        => 'checkbox',
        'section'     => 'epira_section_single_post',
        'transport'   => 'refresh');

    # Social link
    $options['settings'][] = array(
        'settings'    => 'epira_social_link_facebook',
        'label'       => __('Facebook', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_social_link',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_social_link_twitter',
        'label'       => __('Twitter', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_social_link',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_social_link_google_plus',
        'label'       => __('Goggle Plus', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_social_link',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_social_link_pinterest',
        'label'       => __('Pinterest', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_social_link',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_social_link_instagram',
        'label'       => __('Instagram', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_social_link',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_social_link_tumblr',
        'label'       => __('Tumblr', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_social_link',
        'transport'   => 'refresh');

    $options['settings'][] = array(
        'settings'    => 'epira_social_link_rss',
        'label'       => __('Rss', 'epira-lite'),
        'description' => __('enter HIDE if you want to disable this option.', 'epira-lite'),
        'default'     => '',
        'type'        => 'text',
        'section'     => 'epira_section_social_link',
        'transport'   => 'refresh');

    # Custom CSS
    $options['settings'][] = array(
        'settings'    => 'epira_custom_css',
        'label'       => __('Enter your CSS', 'epira-lite'),
        'default'     => '',
        'type'        => 'textarea',
        'section'     => 'epira_section_custom_css',
        'transport'   => 'refresh');

    return $options;
}