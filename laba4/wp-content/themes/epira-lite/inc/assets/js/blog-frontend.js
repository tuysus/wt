"use strict";

jQuery(document).ready(function($){
	var button = jQuery('#kopa-blog-loadmore');
	if(1 === button.length){
		Modernizr.load([{
	        load: [kopa_variable.url.template_directory_uri + 'js/imagesloaded.js', kopa_variable.url.template_directory_uri + 'js/filtermasonry.js', kopa_variable.url.template_directory_uri + 'js/masonry.pkgd.js'],
	        complete: function() {

	        	button.click(function(event) {
	        		event.preventDefault();		        	
	        		
	        		var href = button.data('url');
                	var paged = button.data('paged');

                	if( !button.hasClass('in-process') ){
                		jQuery.ajax({
	                        type: 'POST',
	                        url: href + paged,
	                        dataType: 'html',
	                        data: {},
	                        beforeSend: function() {
	                            button.addClass('in-process');                          
	                        },
	                        success: function(data) {	                        	
                        		var newItems = jQuery(data).find('#kopa-blog-loadmore-placeholder > li');	                        	
	                            
	                            if(newItems.length > 0){
									imagesLoaded(newItems, function () {	  
		                            	var container = jQuery('#kopa-blog-loadmore-placeholder');
		                            	var layout = container.data('layout');

		                            	if('masonry' == layout){
		                            		container.append( newItems ).masonry( 'appended', newItems );	
		                            	}else{
		                            		container.append( newItems );
		                            	}
		                            	
		                            });	   

		                            button.data('paged', parseInt(paged) + 1); 		                            
	                            }	                              
	                        },
	                        complete: function() {   
	                            button.removeClass('in-process');             
	                        },
	                        error: function() {
	                            
	                        }
	                    }).fail(function() { 
	                    	button.remove();
	                    });
                	}

	        	});	

	        }
	    }]);
	}
});