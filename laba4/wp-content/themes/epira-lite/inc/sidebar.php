<?php
   /**
	* Creates a sidebar
	* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
	*/
	$sidebar_main_top_right_args = array(
		'name'          => __( 'Main Top - Right', 'epira-lite' ),
		'id'            => 'sidebar_main_top_right',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '<span class="widget-border-top"></span></div>',
		'before_title'  => '<h2 class="widget-title widget-title-style-5">',
		'after_title'   => '</h2>'
	);

	$sidebar_footer_left_args = array(
		'name'          => __( 'Footer Left', 'epira-lite' ),
		'id'            => 'sidebar_footer_left',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widget-title widget-title-style-7">',
		'after_title'   => '</h5>'
	);

	$sidebar_footer_right_args = array(
		'name'          => __( 'Footer Right', 'epira-lite' ),
		'id'            => 'sidebar_footer_right',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widget-title widget-title-style-7">',
		'after_title'   => '</h5>'
	);

	register_sidebar( $sidebar_main_top_right_args );
	register_sidebar( $sidebar_footer_left_args );
	register_sidebar( $sidebar_footer_right_args );


