<?php 
global $post;

$post_id = get_the_ID($post->ID);
$post_url = get_permalink($post->ID);
$post_title = get_the_title($post->ID);

if(has_post_thumbnail( $post_id )):
	$image = epira_get_image_src($post_id, 'single_post_featured');
?>

	<div class="entry-thumb">
	    <img src="<?php echo esc_url( $image ); ?>" alt="<?php echo $post_title; ?>">
	</div>

<?php 
endif;