<?php get_header(); ?>

<div class="col-a">

    <?php epira_get_breadcrumb(); ?>
    <!-- breadcrumb -->

    <?php
    # get single-post-template info, config by theme-options.

    $is_show_author   = (int) get_theme_mod('epira_single_post_author_box', 1);
    $is_show_related  = (int) get_theme_mod('epira_single_post_related_posts', 1);
    $is_show_comments = (int) get_theme_mod('epira_single_post_comments', 1);

    $col_right_classes = 'clearfix';
    ?>

    <section class="entry-box">
        <?php if (have_posts()): ?>
        	<?php
        	while (have_posts()) : the_post();
                $post_id = get_the_ID();
                $post_url = get_permalink();
                $post_title = get_the_title();
                $post_format = get_post_format();
            	?>
	        <header>
	            <h2 id="kopa-post-title" class="entry-title"><?php echo $post_title; ?></h2>

	            <div class="meta-box clearfix">
                    <span class="entry-author pull-left clearfix"><span class="pull-left"><?php _e('By', 'epira-lite'); ?>&nbsp;</span><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="pull-left"><?php the_author_meta( 'display_name' ); ?></a></span>
                    <span class="entry-date pull-left clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left"><?php echo get_the_date(); ?></span></span>

                    <?php 
                    if(has_category()){                        
                        echo '<span class="entry-categories pull-left">';
                        the_category(', ');
                        echo '</span>';
                    }                                     
                    ?>                    

                </div>
	            <!-- meta-box -->
	      
	        </header>
	        
	        <?php get_template_part( 'layouts/post-featured-content/content', 'featured' ); ?>

	        <div id="kopa-post-content"  <?php post_class( 'entry-content' ); ?>>

	            <div>

                    <div class="clearfix">
					<?php 
					   the_content();
					?>
                    </div>
	                
	            </div>
	            <!-- right-col -->

	            <div class="clear"></div>	            
	            
	        </div>

	        <footer class="clearfix">

	            <?php
                if(has_tag()){
                    $before = sprintf('<div class="tag-box pull-left"><strong>%s</strong>', __('Tags:', 'epira-lite'));
                    $after = '</div>';
                    the_tags( $before, '', $after );
                }
                ?>

                <?php
                wp_link_pages(array(
                    'before' => '<div class="page-links-wrapper pull-right"><div class="page-links">',
                    'after' => '</div></div>',
                    'next_or_number' => 'number',                    
                    'echo' => 1,
                    'pagelink' => '<span>%</span>',
                ));
                ?>               

	        </footer>

	    	<?php endwhile; ?>
    	<?php endif; ?>
    </section>
    <!-- entry-box -->

    <?php 
    if(1 == $is_show_author){
        epira_get_about_author(); 
    }
    ?>
    <!-- about-author -->

    <?php 
    if(1 == $is_show_related):
        $related_articles = epira_get_related_posts();
        $latest_articles = epira_get_latest_posts_by_cat();

        if($related_articles || $latest_articles):
        ?>
        <section id="related-article">

            <div class="row">
                <?php if($related_articles) : ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h5><?php _e('More articles', 'epira-lite'); ?></h5>
                    <?php echo $related_articles; ?>
                </div>
                <?php endif;?>

                <?php if($related_articles) : ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h5><?php _e('Latest articles', 'epira-lite'); ?></h5>
                    <?php echo $related_articles; ?>
                </div>
                <?php endif;?>        
                
            </div>
            <!-- row -->

        </section>
        <!-- related-article -->
        <?php endif; ?>
    <?php endif; ?>

    <?php 
    if(1 == $is_show_comments){
        comments_template(); 
    }
    ?>

</div>
<!-- col-a -->

<div class="sidebar">

    <?php               

        if (is_active_sidebar('sidebar_main_top_right')){            
            dynamic_sidebar('sidebar_main_top_right');
        }                
    ?>

</div>
<!-- sidebar -->

<div class="clear"></div>


<?php 
get_footer();