<?php

define('KOPA_PREFIX', 'epira_');

// require_once( get_template_directory() . '/framework/kopa-framework.php' );
#EPIRA FUNCTION
require_once( get_template_directory() . '/inc/hook.php' );
require_once( get_template_directory() . '/inc/config.php' );

#EPIRA FEATURED
require_once( get_template_directory() . '/inc/sidebar.php' );
// require_once( get_template_directory() . '/inc/widget.php' );
require_once( get_template_directory() . '/lib/customization.php');
require_once( get_template_directory() . '/lib/widget-master.php');

#CUSTOMIZE
require_once( get_template_directory() . '/inc/customize.php' );

get_template_part('inc/widgets/articles', 'videos');
get_template_part('inc/widgets/articles', 'tabs');
get_template_part('inc/widgets/contact', 'information');
get_template_part('inc/widgets/social', 'links');
get_template_part('inc/widgets/articles-with-medium-thumbnail-above', 'content');
// get_template_part('ince/widgets/posts', 'icons');
// get_template_part('ince/widgets/posts', 'only-title');