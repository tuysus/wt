/**
 * Created by tuysus on 24-04-2016.
 */

$(document).ready(function () {

    $("#btnAddUser").click(function () {
        // alert("all ok");
        $("#userEditDlg").modal({
            backdrop : true
        });
    })

    // var wm = new UserWebModel(
    //
    //     $("#userIdDlg").val(),
    //     $("#userNameDlg").val(),
    //     $("#userEmailDlg").val(),
    //     $("#userPasswordDlg").val(),
    //     $("#userPhoneDlg").val()
    // );

    $("#btnUserSaveDlg").click(function () {
        $.post("db.php", {id : $("#userIdDlg").val(), name : $("#userNameDlg").val(), email: $("#userEmailDlg").val(), password : $("#userPasswordDlg").val(), phone : $("#userPhoneDlg").val()}, function(data){
            if(data) {
                viewTable();
            }
            return false;
        });
    })

    viewTable();
});

function bindDeleteClick(elements){
    elements.on("click", function() {
        var userId = $(this).parents("tr").attr("trId");
        $.post("del.php", {id:userId}, function(data) {
            viewTable();
            return false;
        });
    })
}


// function UserWebModel(id, name, email, password, phone) {
//     this.id = id;
//     this.name = name;
//     this.email = email;
//     this.password = password;
//     this.phone = phone;
// }

function viewTable() {
    $.post("table.php", "", function (result){
        var data = JSON.parse(result)
        $("#tblUsers").children("tbody").html(function () {
            var strtable = "";
            for(var i = 0; i < data.length; i++){
                strtable += "<tr trID = " + data[i].USER_ID + ">";

                strtable += "<td class = 'count'>" + i + "</td>";
                strtable += "<td class = 'name'>" +  data[i].NAME + "</td>";
                strtable += "<td class = 'email'>" +  data[i].EMAIL + "</td>";
                strtable += "<td class = 'phone'>" +  data[i].PHONE + "</td>";

                strtable += "<td class = 'delete' userId =" + data[i].USER_ID + ">" +
                    "<div style='width: 100px'>"
                    +
                    "<button type='button' class='btn btn-default userDelete' title='Удалить'>" +
                    "<span class='glyphicon glyphicon-trash'></span>" +
                    "</button>"
                    +
                    "</div>"
                    +
                    "</td>";
                strtable += "</tr>";
            }
            return strtable;
        });
        bindDeleteClick( $('#tblUsers .userDelete') );
        return false;
    })
}